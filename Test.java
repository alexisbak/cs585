package src;

import java.sql.*;

public class Test {

	static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static String DB_URL = "jdbc:mysql://127.0.0.1/noura";

	static String USER = "root";
	static String PASS = "blue1986";
	static String name = null;
	static String sql;
	static Connection conn = null;
	static Statement stmt = null;
	static ResultSet rs;
	static int account;

	public static void main(String[] args) throws Throwable {

		Class.forName(JDBC_DRIVER);

		conn = DriverManager.getConnection(DB_URL, USER, PASS);

		stmt = conn.createStatement();

		name = EnterUserName();

		sql = "SELECT account FROM inject WHERE username= '" + name + "'";

		rs = stmt.executeQuery(sql);

		while (rs.next()) {

			account = rs.getInt("account");

			System.out.println("   Account No. " + account);
		}

		name = GetCleanUserName();

		sql = "SELECT account FROM inject WHERE username= '" + name + "'";

		rs = stmt.executeQuery(sql);

		while (rs.next()) {

			account = rs.getInt("account");

			System.out.println("   Account No. " + account);
		}

		rs.close();
		stmt.close();
		conn.close();

		System.out.println("Goodbye!");
	}

	@Dirty
	public static String EnterUserName() {
		return "Alex;--";
	}

	@Clean
	public static String GetCleanUserName() {
		return "Alex";
	}

}
