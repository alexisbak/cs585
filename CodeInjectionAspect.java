package src;

import java.util.ArrayList;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class CodeInjectionAspect {

	private ArrayList<String> dirtyList = new ArrayList<String>();
	private ArrayList<DirtyLine> dirtylines = new ArrayList<DirtyLine>();

	int lineN = -1;
	String mName, fName = "";
	boolean isDirtyVar, isDirtyMethod = false;

	@Pointcut("!within(CodeInjectionAspect) && withincode(* Test.*(..))")
	public void getMyStatements() {
	}

	@Before("getMyStatements()")
	public void goThroStatements(JoinPoint jp) {

		int curline = jp.getSourceLocation().getLine();

		if (jp.getKind().equalsIgnoreCase(JoinPoint.FIELD_SET) && isDirtyMethod
				&& curline == lineN) {
			if (!dirtyList.contains(jp.getSignature().getName())) {
				dirtyList.add(jp.getSignature().getName());
				addDirtyLine(jp.getSignature().getName(), curline);
			}
			clearInfo_M();
		}

		else if (jp.getKind().equalsIgnoreCase(JoinPoint.FIELD_SET)
				&& !isDirtyMethod && curline == lineN) {
			if (dirtyList.contains(jp.getSignature().getName())) {
				dirtyList.remove(jp.getSignature().getName());
				removeDirtyLine(jp.getSignature().getName());
			}
			clearInfo_M();
		}

		else if (jp.getKind().equalsIgnoreCase(JoinPoint.FIELD_SET)
				&& lineN == curline && isDirtyVar) {
			if (!dirtyList.contains(jp.getSignature().getName())) {
				dirtyList.add(jp.getSignature().getName());
				addDirtyLine(jp.getSignature().getName(), curline);
			}
			clearInfo_F();
		}

		else if (jp.getKind().equalsIgnoreCase(JoinPoint.FIELD_SET)
				&& lineN == curline && !isDirtyVar) {
			if (dirtyList.contains(jp.getSignature().getName())) {
				dirtyList.remove(jp.getSignature().getName());
				removeDirtyLine(jp.getSignature().getName());
			}
			clearInfo_F();
		}

		if (jp.getKind().equalsIgnoreCase(JoinPoint.FIELD_GET)
				&& dirtyList.contains(jp.getSignature().getName())) {
			isDirtyVar = true;
			recordInfo_F(jp);
		}

		else if (jp.getKind().equalsIgnoreCase(JoinPoint.FIELD_GET)
				&& !dirtyList.contains(jp.getSignature().getName())) {
			if (lineN == curline && isDirtyVar) {
			} else {
				isDirtyVar = false;
				recordInfo_F(jp);
			}
		}
	}

	@Around("@annotation(Maybe)")
	public Object analyzeMaybeMethods(ProceedingJoinPoint pjp) throws Throwable {

		int curLine = pjp.getSourceLocation().getLine();
		Object result = null;

		if (!isDirtyVar && curLine == lineN) {
			lineN = pjp.getSourceLocation().getLine();
			mName = pjp.getSignature().getName();
			isDirtyMethod = false;

		} else if (isDirtyVar && curLine == lineN) {
			lineN = pjp.getSourceLocation().getLine();
			mName = pjp.getSignature().getName();
			isDirtyMethod = true;
			System.out.println("Running " + "<" + mName + ">"
					+ " with argument " + "<" + fName + "> "
					+ "can be dangerous!");
			System.out.println("Please check line at " + "<" + getLine(fName)
					+ ">!!");
		}
		result = pjp.proceed();
		return result;
	}

	@Around(value = "@annotation(Dirty)")
	public Object analyzeDirtyMethods(ProceedingJoinPoint pjp) throws Throwable {
		
		isDirtyMethod = true;
		Object result = null;
		recordInfo_M(pjp);

		int currentline = pjp.getSourceLocation().getLine();
		
		if (isDirtyVar && currentline == lineN) {
			System.out.println("Running " + "<" + pjp.getSignature().getName()
					+ ">" + " with argument " + "<" + fName + "> "
					+ "can be dangerous!");
			System.out.println("Please check line at " + "<" + getLine(fName)
					+ ">!!");
		}
		result = pjp.proceed();
		return result;
	}

	@Around(value = "@annotation(Clean)")
	public Object analyzeCleanMethods(ProceedingJoinPoint pjp) throws Throwable {
		
		Object result = null;
		int currentline = pjp.getSourceLocation().getLine();
		
		if (isDirtyVar && currentline == lineN) {
			isDirtyMethod = true;
			recordInfo_M(pjp);
			
			System.out.println("Running " + "<" + pjp.getSignature().getName()
					+ ">" + " with argument " + "<" + fName + "> "
					+ "can be dangerous!");
			System.out.println("Please check line at " + "<" + getLine(fName)
					+ ">!!");
		
		} else if (!isDirtyVar && currentline == lineN) {
			isDirtyMethod = false;
			recordInfo_M(pjp);
		}
		result = pjp.proceed();
		return result;
	}

	private void recordInfo_M(JoinPoint jp) {
		lineN = jp.getSourceLocation().getLine();
		mName = jp.getSignature().getName();
	}

	private void recordInfo_F(JoinPoint jp) {
		lineN = jp.getSourceLocation().getLine();
		fName = jp.getSignature().getName();
	}

	private void clearInfo_M() {
		lineN = -1;
		mName = "";
		isDirtyMethod = false;
	}

	private void clearInfo_F() {
		lineN = -1;
		fName = "";
		isDirtyVar = false;
	}

	class DirtyLine {
		public String name;
		public int line;

		public DirtyLine() {
			name = "";
			line = -1;
		}

		public DirtyLine(String name, int line) {
			this.name = name;
			this.line = line;
		}
	}

	private boolean lineExists(String name) {
		if (!dirtylines.isEmpty()) {
			for (int i = 0; i < dirtylines.size(); i++) {
				if (dirtylines.get(i).name.equalsIgnoreCase(name)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean removeDirtyLine(String name) {
		if (!dirtylines.isEmpty()) {
			for (int i = 0; i < dirtylines.size(); i++) {
				if (dirtylines.get(i).name.equalsIgnoreCase(name)) {
					dirtylines.remove(i);
					return true;
				}
			}
		}
		return false;

	}

	private boolean addDirtyLine(String name, int line) {
		if (!dirtylines.isEmpty()) {
			for (int i = 0; i < dirtylines.size(); i++) {
				if (dirtylines.get(i).name.equalsIgnoreCase(name)) {
					return false;
				}
			}
		}
		dirtylines.add(new DirtyLine(name, line));
		return true;
	}

	private int getLine(String name) {
		if (!dirtylines.isEmpty()) {
			for (int i = 0; i < dirtylines.size(); i++) {
				if (dirtylines.get(i).name.equalsIgnoreCase(name)) {
					return dirtylines.get(i).line;
				}
			}
		}
		return -1;
	}
}
